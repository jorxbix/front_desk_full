package com.example.frontdesk;

import org.json.JSONObject;

import java.util.Date;

class Reserva {

    private int num_huespedes;
    private Huesped[] arr_huespedes;
    private long id_reserva;
    private Date fecha_entrada;
    private Date fecha_salida;
    private String canal;


    //??Ocupacion....Autogenerado??

    //Constructor Reservas a partir de archivo Json:
    public Reserva(JSONObject mi_json){

    }


    //GETTERS Y SETTERS
    public long getId_reserva() {
        return id_reserva;
    }

    public void setId_reserva(long id_reserva) {
        this.id_reserva = id_reserva;
    }

    public Date getFecha_entrada() {
        return fecha_entrada;
    }

    public void setFecha_entrada(Date fecha_entrada) {
        this.fecha_entrada = fecha_entrada;
    }

    public Date getFecha_salida() {
        return fecha_salida;
    }

    public void setFecha_salida(Date fecha_salida) {
        this.fecha_salida = fecha_salida;
    }

    public String getCanal() {
        return canal;
    }

    public void setCanal(String canal) {
        this.canal = canal;
    }
}
